
function Display16seg(dispAdr, i2cBus){
    this.i2c1 = i2cBus;
    this.displayAddress = dispAdr;
}


Display16seg.prototype.clearDisplay = function(){
    var buf = Buffer.from('05', 'hex');

	this.i2c1.i2cWriteSync(this.displayAddress, buf.length, buf);
//	this.fs.writeSync(0, 'error: ' +e.toString() + '\n');
}

Display16seg.prototype.displayBrightness = function(level){
    var newBrightness ="06";
    if (level < 16){
	newBrightness += "0";
    }
    newBrightness += level.toString(16);
    var buf = Buffer.from(newBrightness, 'hex');
	this.i2c1.i2cWriteSync(this.displayAddress, buf.length, buf);
}

Display16seg.prototype.writeDisplay = function(dispString){
    var newString = "01";
    for(i in dispString){
	var x = dispString.charCodeAt(i);
	if(x<16){
	    newString += "0";
	}
	newString += x.toString(16);
    }
    var buf = Buffer.from(newString, 'hex');
	this.i2c1.i2cWriteSync(this.displayAddress, buf.length, buf);
}
Display16seg.prototype.dot = function(n, i){
    var newString = '030';
    newString += n.toString(16);
    newString += '0';
    newString += i.toString(16);
    var buf = Buffer.from(newString, 'hex');
	this.i2c1.i2cWriteSync(this.displayAddress, buf.length, buf);
}

Display16seg.prototype.writeRaw = function(a, b, c, d){
    var newString = '02';
    var bits = [];
    bits[0] = Math.floor(a/256);
    bits[2] = Math.floor(b/256);
    bits[4] = Math.floor(c/256);
    bits[6] = Math.floor(d/256);
    bits[1] = a % 256;
    bits[3] = b % 256;
    bits[5] = c % 256;
    bits[7] = d % 256;
    for(i of bits){
        if(i < 16){
            newString += '0';
        }
        newString += i.toString(16);
    }
    var buf = Buffer.from(newString, 'hex');
	this.i2c1.i2cWriteSync(this.displayAddress, buf.length, buf);
}

Display16seg.prototype.test = function(i){
    var newString = '000';
    newString += i.toString(16);
	this.i2c1.i2cWriteSync(this.displayAddress, buf.length, buf);
}


exports.dsp16s04h = Display16seg;
